# The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored _approximate
zstyle ':completion:*' format ' - ~%d'
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' matcher-list '' 'r:|[._-]=** r:|=**' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'l:|=* r:|=*'
zstyle :compinstall filename '/home/sean/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install

# dotnet tools path
export DN_TOOLS='/home/sean/.dotnet/tools'


# use vim for sudoedit
export SUDO_EDITOR=vim

source ~/.profile

# Shell aliases
alias reso='source ~/.zshrc'
alias vi_zshrc='vim ~/.zshrc'
alias vi_nixconf='sudoedit vim /etc/nixos/configuration.nix'
alias vi_tmux='vim ~/.tmux.conf'
alias elm-app='/home/sean/.npm/node-stuff/bin/elm-app'
alias elm-markup='/home/sean/.npm/node-stuff/bin/elm-markup'
alias paket='sudo /root/.dotnet/tools/paket'
alias curlJ="curl -H 'Content-Type:application/json' -d"

# git aliases
alias gs='git status'
alias gfap='git fetch --all -p'
alias gdh='git diff HEAD'
alias gdhc='git diff HEAD --cached'
alias gp='git push'
alias gc='git checkout'
alias gcb='git branch | grep "*" | cut -d" " -f2'
alias gsf='gs -suno'

# ssh
alias ags='eval $(ssh-agent)'
alias aga='ssh-add'
alias aga-gitlab='ssh-add ~/.ssh/gitlab-personal'
alias aga-thenewells='ssh-add ~/.ssh/do-thenewells'

alias spotify='flatpak run com.spotify.Client'

# npm
alias nise='npm i --save-exact' 

# curl
# curl -b cookies.txt -c cookies.txt -H "Content-Type: application/json" -X POST -d '{"email":"test@test.com","password":"pass"}' http://localhost:8080/login
alias ccp='curl -b cookies.txt -c cookies.txt -H "Content-Type: application/json" -X POST'
alias ccg='curl -b cookies.txt -c cookies.txt -H "Content-Type: application/json"'

# git functions
function gcom() {
  if [ -n "$1" ]
  then
    git commit -m "$1"
  else
    git commit
  fi
}

alias gcomYT=gcom

# commit them push
function gcomP() {
  gcom $1
  gp
}

# trakr dev funtions
function dexTrakr() {
  docker exec trakr-postgres-db psql -U trakrapp -d postgres -c "$1"
}

function dexPostgres() {
  docker exec trakr-postgres-db psql -U postgres -d postgres -c "$1"
}

