set nocompatible

set rnu
set ruler
let mapleader=" "

map <Leader><Space> :noh<CR>

filetype indent plugin on

syntax on

set term=xterm
set background=dark
colorscheme desert

set wildmenu
set hlsearch

" Use case insensitive search, except when using capital letters
set ignorecase
set smartcase
 
" Allow backspacing over autoindent, line breaks and start of insert action
set backspace=indent,eol,start
 
" When opening a new line and no filetype-specific indenting is enabled, keep
" the same indent as the line you're currently on. Useful for READMEs, etc.
set autoindent

